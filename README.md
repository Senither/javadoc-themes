# Javadoc Themes

Give your boring old javadocs a fresh new look!

## How to Use

You can either manually update your javadoc CSS, or you can automate the process by using a stylesheet in a command line argument to generate the javadoc with the new stylesheet.

If you wanna do it manually, just go to any theme you want, click `raw` and copy and paste it into your `stylesheet.css` file.

To automate the process, follow these steps:

### Step 1

Well, basically write javadoc style comments in your java source code.

### Step 2

Go to any theme you want and download it to your computer, or clone the entire repository to a folder on your local computer.

### Step 3

Open your favorite editor and open the Javadoc generation options, depending on what editor you're using this will differ.

### Step 4

Locate the option that allows you to parse in command line arguments, and provided the following argument.

`-stylesheetfile <location_of_the_stylesheet_directory>/stylesheet.min.css`

### Step 5 

And that's all, just hit Ok and generated your javadocs.

## Pull Requests

I welcome and encourage all pull requests. It will usually take me within 24-48 hours to respond to any issue or request.

## Created & Maintained By

[Alexis Tan](https://bitbucket.org/Senither) ([@senither](http://senither.com/))

## License

```
Copyright (c) 2016 Alexis Tan <alexis@sen-dev.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```